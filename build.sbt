name := "pps2017-asg03-prolog"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  //Scala Test libraries
  "org.scalactic" %% "scalactic" % "3.0.5" % Test,
  "org.scalatest" %% "scalatest" % "3.0.5" % Test,

  "it.unibo.alice.tuprolog" % "tuprolog" % "3.3.0",
)

unmanagedResourceDirectories in Compile += { baseDirectory.value / "src/main/prolog" }
unmanagedResourceDirectories in Test += { baseDirectory.value / "src/test/prolog" }