%% Predicates to extract a term from the k-d tree

extract_node(tree(_, Node, _), Node).

extract_pattern(node(Pattern), Pattern).
extract_pattern(node(Pattern, _), Pattern).

extract_cut(node(_, cut(Attribute, Value)), [Attribute, Value]).

extract_left_tree(tree(LeftSubTree, _, _), LeftSubTree).
extract_right_tree(tree(_, _, RightSubTree), RightSubTree).