%% median(+List, +Position, -Median, -Left, -Right)
%
%		The median/5 predicate finds the median element of the list and two detected partitions.
%
%		@arg List			List of numeric values.
%		@arg Position	Index of the nested list with which compare each element of the external list.
%		@arg Median		Element in the middle (approximately) of the list
%		@arg Left			Partition of the data points with the value in Position
%										lower than the value in Position of the Median.
%		@arg Right		Partition of the data points with the value in Position
%										greater than the value in Position of the Median.

median(List, Position, Median, Left, Right) :-
	member(Median, List),
	partition(List, Median, Position, Left, Right),
	length(Left, LeftLength),
    length(Right, RightLength),
    differ_by(LeftLength, RightLength, 1), !.


%% partition(+List, +Pivot, +Position, -Left, -Right)
%
%		The partition/5 predicate finds two partitions using the pivot.
%
%		@arg List			List of numeric values.
%		@arg Pivot		Element witch which to partition the list.
%		@arg Position	Index of the nested list with which compare each element of the external list.
%		@arg Left			Partition of the data points with the value in Position
%										lower than the value in Position of the Pivot.
%		@arg Right		Partition of the data points with the value in Position
%										greater than the value in Position of the Pivot.

partition([], _, _, [], []).
partition([Pivot | Tail], Pivot, Position, Left, Right) :- !,
    partition(Tail, Pivot, Position, Left, Right).

partition([Head | Tail], Pivot, Position, [Head | Left], Right) :-
	element(Position, Head, X),
	element(Position, Pivot, Y),
	X < Y, !,
	partition(Tail, Pivot, Position, Left, Right).

partition([Head | Tail], Pivot, Position, Left, [Head | Right]) :-
	partition(Tail, Pivot, Position, Left, Right).
