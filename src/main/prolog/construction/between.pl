%% between(+Low, +Value, +High)
%
%		The between/3 predicate checks if the value is strictly
%		between the lower limit and the upper limit.
%
%		@arg Low	Lower limit.
%		@arg Value	Value to be verified.
%		@arg High	Higher limit.

between(Low, Value, High) :-
    Value >= Low, Value =< High.