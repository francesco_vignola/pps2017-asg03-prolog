%% squared_standard_deviation(+Values, -Variance)
%
%		The squared_standard_deviation/2 predicate measures how far a set of numbers
%		are spread out from their average value.
%
%
%		@arg Values		List of numeric values.
%		@arg Variance	Numerical value that describes the variance.

squared_standard_deviation(List, Variance) :-
	length(List, Length),
	squared_deviations_sum(List, 0, Length, 0, _, Deviation),
	Variance is Deviation / Length.

squared_deviations_sum([], Sum, Length, Current_Deviation, Mean, Current_Deviation) :-
	Mean is Sum / Length.

squared_deviations_sum([Head | Tail], Sum, Length, Current_Deviation, Mean, Deviation) :-
	Current_Sum is Head + Sum,
	squared_deviations_sum(Tail, Current_Sum, Length, Current_Deviation, Mean, Dev),
	Deviation is Dev + ((Head - Mean) ** 2).
