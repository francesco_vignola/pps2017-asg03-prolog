
:- include('median.pl').
:- include('between.pl').
:- include('differ_by.pl').
:- include('transpose_of_matrix.pl').
:- include('partitioning_attribute.pl').
:- include('squared_standard_deviation.pl').


%% kd_tree(+Patterns, -Kd_Tree)
%
%		The kd_tree/2 predicate constructs a k-dimensional tree from a list of patterns.
%		KD tree is a space-partitioning data structure for organizing points in a k-dimensional space.
%
%		More details about k-d tree:
%			- https://en.wikipedia.org/wiki/K-d_tree
%			- https://www.analyticsvidhya.com/blog/2017/11/information-retrieval-using-kdtree/
%
%		@arg Patterns   List of data points/vectors.
%		@arg Kd_Tree    Binary Space Partitioning Tree.

kd_tree([], nil).
kd_tree([Head], tree(nil, node(Head), nil)).
kd_tree(Patterns, tree(Left_Subtree, node(Pattern, cut(Attribute, Value)), Right_Subtree)) :-
	dimensional_partition(Patterns, Attribute, Value, Pattern, Left, Right),
	kd_tree(Left, Left_Subtree),
	kd_tree(Right, Right_Subtree), !.


%% dimensional_partition(+Patterns, -Attribute, -Value, -Pattern, -Left, -Right)
%
%		The dimensional_partition/6 predicate separates the list of patterns into two balanced partitions,
%		called Left and Right, also returning the median item.
%
%		@arg Patterns	List of data points.
%		@arg Attribute	A dimension of space, that is a coordinate of the data vector.
%		@arg Value		Value of the Attribute.
%		@arg Pattern	Data point that divides the two partitions.
%		@arg Left		Partition of data points with Attribute value
%						smaller than the Attribute value of the Pattern.
%		@arg Right		Partition of data points with Attribute value
%						greater than the Attribute value of the Pattern.

dimensional_partition(Patterns, Attribute, Value, Pattern, Left, Right) :-
	partitioning_attribute(Patterns, Attribute),
	median(Patterns, Attribute, Pattern, Left, Right),
	element(Attribute, Pattern, Value).
