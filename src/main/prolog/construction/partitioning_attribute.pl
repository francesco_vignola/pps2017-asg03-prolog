%% partitioning_attribute(+Patterns, -Attribute)
%
%		The partitioning_attribute/2 predicate finds the Attribute with which to partition the space.
%
%
%		@arg Patterns	List of data points/vectors.
%		@arg Attribute	A dimension of space, that is a coordinate of the data vector.

partitioning_attribute(Patterns, Attribute) :-
	transpose_of_matrix(Patterns, Features),
	attribute_with_maximum_variance(Features, Attribute).


%% attribute_with_maximum_variance(+Features, -Attribute)
%
%		The attribute_with_maximum_variance/2 predicate finds the Attribute with maximum variance.
%
%
%		@arg Features	List of values for each attribute.
%		@arg Attribute	A dimension of space, that is a coordinate of the data vector.

attribute_with_maximum_variance([Head | Tail], Attribute) :-
	squared_standard_deviation(Head, Variance),
	attribute_with_maximum_variance(Tail, 1, Variance, 1, Attribute).

attribute_with_maximum_variance([], _, _, Current_Max_Attribute, Current_Max_Attribute).
attribute_with_maximum_variance([Head | Tail], Previous_Attribute, Current_Max_Variance, Current_Max_Attribute, Max_Attribute) :-
	Attribute is Previous_Attribute + 1,
	squared_standard_deviation(Head, Variance),
	(Variance > Current_Max_Variance ->
	attribute_with_maximum_variance(Tail, Attribute, Variance, Attribute, Max_Attribute);
	attribute_with_maximum_variance(Tail, Attribute, Current_Max_Variance, Current_Max_Attribute, Max_Attribute)).
