%% transpose_of_matrix(+Matrix, -Transposed_Matrix)
%
%		The transpose_of_matrix/2 predicate finds the Attribute with which to partition the space.
%
%
%		@arg Matrix				List of rows.
%		@arg Transposed_Matrix	Matrix whose rows are the columns of the original.

transpose_of_matrix([], []).
transpose_of_matrix([[] | _], []).
transpose_of_matrix(Matrix, [Head | Tail]) :-
	columns(Matrix, Head, Columns),
	transpose_of_matrix(Columns, Tail).

columns([], [], []).
columns([[Item11 | Items1n] | Tail], [Item11 | ItemsX1], [Items1n | ItemsXn]) :-
	columns(Tail, ItemsX1, ItemsXn).