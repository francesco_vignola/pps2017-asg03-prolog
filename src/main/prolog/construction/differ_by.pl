%% differ_by(+Value1, +Value2, + Higher_Limit)
%
%		The differ_by/2 predicate checks whether the two heights are the same or only differ by HigherLimit.
%
%		@arg Value1         First value.
%		@arg Value2         Second value.
%       @arg Higher_Limit   Maximum difference accepted between the two values.

differ_by(Value1, Value2, Higher_Limit) :-
    Difference is abs(Value1 - Value2),
    between(0, Difference, Higher_Limit).