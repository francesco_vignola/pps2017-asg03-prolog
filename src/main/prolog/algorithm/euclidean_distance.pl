%% euclidean_distance(+Query_Point, +Features, -Distance)
%
%       The euclidean_distance/3 predicate calculates the
%       geometric distance between two data points.
%
%       @arg Query_Point    The data point for which to find the K neighbors.
%       @arg Features       List of numbers representing the pattern.
%       @arg Distance       The distance between two data points.

euclidean_distance(Query_Point, Features, Distance) :-
	residual_sum_of_squares(Query_Point, Features, RSS),
	Distance is sqrt(RSS).

residual_sum_of_squares([], [], 0) :- !.
residual_sum_of_squares([FQ | FsQ], [FN | FsN], RSS) :-
	residual_sum_of_squares(FsQ, FsN, CRSS),
	R is FQ - FN,
	RS is R ** 2,
	RSS is CRSS + RS.