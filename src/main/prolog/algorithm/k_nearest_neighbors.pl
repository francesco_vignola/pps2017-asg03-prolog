
:- include('min.pl').
:- include('nodes.pl').
:- include('features.pl').
:- include('quicksort.pl').
:- include('visit_order.pl').
:- include('are_less_than.pl').
:- include('evaluate_node.pl').
:- include('evaluate_subtree.pl').
:- include('euclidean_distance.pl').
:- include('intersect_target_area.pl').


%% k_nearest_neighbors(+K, +Query_Point, +Kd_Tree, Nearest_Neighbors)
%
%		The k_nearest_neighbors/4 is an auxiliary predicate useful
%       for controlling K, before searching for neighbors.
%
%		@arg K                  The number of neighbors to find.
%		@arg Query_Point        The data point for which to find the K neighbors.
%		@arg Kd_Tree            The k-d tree to traverse.
%		@arg Nearest_Neighbors  The K nearest neighbors found.

k_nearest_neighbors(K, Query_Point, Kd_Tree, Nearest_Neighbors) :-
	nodes(Kd_Tree, Nodes),
	K =< Nodes,
	once(knn(K, Query_Point, Kd_Tree, [], Nearest_Neighbors)).


%% knn(+K, +Query_Point, +Kd_Tree, +Current_Neighbors, -Nearest_Neighbors)
%
%		The knn/5 predicate to find the K nearest neighbors.
%       The search takes place by going down the tree, comparing the value
%       of the splitting attribute with that of the query_point.
%       During the search, some sub-trees are pruned to maximize the
%       efficiency of the search.
%
%		@arg K                  The number of neighbors to find.
%		@arg Query_Point        The data point for which to find the K neighbors.
%		@arg Kd_Tree            The k-d tree to traverse.
%		@arg Current_Neighbors  Accumulator of neighbors currently found.
%		@arg Nearest_Neighbors  The K nearest neighbors found.

knn(_, _, nil, Current_Neighbors, Current_Neighbors).
knn(K, Query_Point, tree(nil, node(Features), nil), Current_Neighbors, Nearest_Neighbors) :-
	evaluate_node(K, Query_Point, Features, Current_Neighbors, Nearest_Neighbors).

knn(
	K, Query_Point,
	tree(Left_Tree, node(Features, Cut_Dimension), Right_Tree),
	Current_Neighbors, Nearest_Neighbors
) :-
	evaluate_node(K, Query_Point, Features, Current_Neighbors, Nearest_Neighbors_Here),

	order_to_visit(Query_Point, Cut_Dimension, Left_Tree, Right_Tree, First_Tree, Second_Tree),
	knn(K, Query_Point, First_Tree, Nearest_Neighbors_Here, Nearest_Neighbors_In_Fall),
	evaluate_subtree(K, Query_Point, Features, Second_Tree, Nearest_Neighbors_In_Fall, Nearest_Neighbors).
