%% evaluate_subtree(+K, +Query_Point, +Features, +Tree, +Current_Nearest_Neighbors, -Nearest_Neighbors)
%
%		The evaluate_subtree/6 predicate traverse the tree if certain conditions are satisfied.
%
%		@arg K                          The number of neighbors to find.
%		@arg Query_Point                The data point for which to find the K neighbors.
%		@arg Features                   List of numbers representing the pattern.
%		@arg Tree                       The k-d tree to traverse.
%		@arg Current_Nearest_Neighbors  Accumulator of neighbors currently found.
%		@arg Nearest_Neighbors          The K nearest neighbors found.

evaluate_subtree(K, Query_Point, _, Tree, Current_Nearest_Neighbors, Nearest_Neighbors) :-
	are_less_than(Current_Nearest_Neighbors, K),
	!,
	knn(K, Query_Point, Tree, Current_Nearest_Neighbors, Nearest_Neighbors).

evaluate_subtree(K, Query_Point, Features, Tree, Current_Nearest_Neighbors, Nearest_Neighbors) :-
	intersect_target_area(Query_Point, Features, Tree, Current_Nearest_Neighbors),
	!,
	knn(K, Query_Point, Tree, Current_Nearest_Neighbors, Nearest_Neighbors).

evaluate_subtree(_, _, _, _, Current_Nearest_Neighbors, Current_Nearest_Neighbors).