%% intersect_target_area(+Query_Point, +Features, +Tree, +Current_Neighbors)
%
%		The intersect_target_area/4 predicate evaluates whether the tree
%       intersects the data point area.
%
%		@arg Query_Point        The data point for which to find the K neighbors.
%		@arg Features           List of numbers representing the pattern.
%		@arg Tree               The k-d tree to traverse.
%		@arg Current_Neighbors  Accumulator of neighbors currently found.

intersect_target_area(Query_Point, Features, Tree, Current_Neighbors) :-
	features(Tree, Features_Second),
	quicksort(Current_Neighbors, [(Far_Distance, _) | _ ]),
	euclidean_distance(Query_Point, Features, Distance),
	(
	    Distance =< Far_Distance
	    ; (
			euclidean_distance(Query_Point, Features_Second, Distance_Second),
			min(Distance, Far_Distance, Distance),
			min(Distance_Second, Far_Distance, Far_Distance)
		  )
	).