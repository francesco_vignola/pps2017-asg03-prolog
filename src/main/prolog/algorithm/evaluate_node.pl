%% evaluate_node(+K, +Query_Point, +Features, +Current_Neighbors, -Nearest_Neighbors)
%
%		The evaluate_node/5 predicate adds the node as neighbor if specific conditions are satisfied.
%
%		@arg K                  The number of neighbors to find.
%		@arg Query_Point        The data point for which to find the K neighbors.
%		@arg Features           List of numbers representing the pattern.
%		@arg Current_Neighbors  Accumulator of neighbors currently found.
%		@arg Nearest_Neighbors  The K nearest neighbors found.

evaluate_node(K, Query_Point, Features, Current_Neighbors, [(Distance, Features) | Current_Neighbors]) :-
	are_less_than(Current_Neighbors, K),
	!,
	euclidean_distance(Query_Point, Features, Distance).

evaluate_node(_, Query_Point, Features, Current_Neighbors, [(Distance, Features) | Sorted_Neighbors]) :-
    quicksort(Current_Neighbors, [(Far_Distance, _) | Sorted_Neighbors]),
    euclidean_distance(Query_Point, Features, Distance),
    Distance =< Far_Distance,
    !.

evaluate_node(_, _, _, Current_Neighbors, Current_Neighbors).