%% are_less_than(+Nearest_Neighbors, +K)
%
%       The are_less_than/2 predicate checks if
%       the nearest neighbors are less than K.
%
%       @arg Nearest_Neighbors  The nearest neighbors found.
%       @arg K                  The number of neighbors to find.

are_less_than(Nearest_Neighbors, K) :-
	length(Nearest_Neighbors, Neighbors_Found),
	Neighbors_Found < K.