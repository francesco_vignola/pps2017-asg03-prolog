%% nodes(+Kd_Tree, -Nodes)
%
%       The nodes/2 predicate provides the number of nodes of the tree.
%
%       @arg Kd_Tree    The tree from which to count the nodes.
%       @arg Nodes      The number of nodes that the tree contains.

nodes(nil, 0) :- !.
nodes(tree(nil, node(_), nil), 1) :- !.
nodes(tree(Left_Tree, node(_, _), Right_Tree), Nodes) :-
	nodes(Left_Tree, Left_Nodes),
	nodes(Right_Tree, Right_Nodes),
	Nodes is Left_Nodes + Right_Nodes + 1.