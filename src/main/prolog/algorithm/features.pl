%% features(+Kd_Tree, -Features)
%
%       The features/2 predicate provides the tree root features.
%
%       @arg Kd_Tree    The k-d tree to traverse.
%       @arg Features   List of numbers representing the pattern.

features(nil, []).
features(tree(_, node(Features), _), Features).
features(tree(_, node(Features, _), _), Features).