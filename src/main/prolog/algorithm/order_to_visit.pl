%% order_to_visit(+Query_Point, +Cutting_Dimension, +Left_Tree, +Right_Tree, -First_Tree, -Second_Tree)
%
%		The order_to_visit/6 predicate to determine the order in which to visit the two trees.
%
%		@arg Query_Point        The data point for which to find the K neighbors.
%		@arg Cutting_Dimension  The dimension with which the tree was divided and
%                               useful for deciding which subtree to visit.
%		@arg Left_Tree          The left tree.
%		@arg Right_Tree         The right tree.
%		@arg First_Tree         The first tree to visit.
%		@arg Second_Tree        The second tree to visit.

order_to_visit(Query_Point, cut(Attribute, Value), Left_Tree, Right_Tree, Left_Tree, Right_Tree) :-
	element(Attribute, Query_Point, Query_Point_Value),
	Query_Point_Value < Value,
	!.

order_to_visit(_, _, Left_Tree, Right_Tree, Right_Tree, Left_Tree).