%% quicksort(+List, -Sorted_List)
%
%		The quicksort/2 predicate sorts the tuples based
%       on the first element.
%
%		@arg List           The list of tuples to sort.
%		@arg Sorted_List    The sorted list.

quicksort(List, Sorted_List) :-
	quick_sort(List, [], Sorted_List).

quick_sort([], Accumulator, Accumulator).
quick_sort([Head | Tail], Accumulator, Sorted_List) :-
	partition(Tail, Head, Left, Right),
	quick_sort(Left, Accumulator, Sorted_Left),
	quick_sort(Right, [Head | Sorted_Left], Sorted_List).


%% partition(+List, +Pivot, -Left, -Right)
%
%		The partition/4 predicate XXX.
%
%		@arg List   The list of tuples to sort.
%		@arg Pivot  Element that divides the two sublists.
%       @arg Left   The partition of the list with
%                   smaller elements of the Pivot.
%       @arg Right  The partition of the list with
%                   greater elements of the Pivot.

partition([], _, [], []).
partition([Y | Xs], Y, Ls, Bs) :-
	!,
	partition(Xs, Y, Ls, Bs).

partition([(D_X, F_X) | Xs], (D_Y, F_Y), [(D_X, F_X) | Ls], Bs) :-
	D_X < D_Y,
	!,
	partition(Xs, (D_Y, F_Y), Ls, Bs).

partition([X | Xs], Y, Ls, [X | Bs]) :-
	partition(Xs, Y, Ls, Bs).