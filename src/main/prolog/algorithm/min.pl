%% min(+Number_One, +Number_Two, ?Minimum_Number)
%
%       The min/3 predicate provides the minimum number between the two data.
%       Furthermore, this predicate checks if the minimum number entered is correct.
%
%       @arg Number_One     Number to compare.
%       @arg Number_Two     Number to compare.
%       @arg Minimum_Number The minimum number between the two.

min(X, Y, X) :-
	X < Y.

min(X, Y, Y) :-
	X > Y.