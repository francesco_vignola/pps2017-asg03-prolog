import java.nio.file.Paths

import ScalaToProlog._
import alice.tuprolog.{Struct, Term, Theory, Var}

trait PrologEngine extends TheoryLoader {
  protected val pathTheories: String

  protected def satisfyGoal(parameter: Goal, inputArgument: Term): Option[Term] = {
    val goal: Struct = new Struct(parameter.predicateName, inputArgument, new Var(parameter.variableName))
    solveOneAndGetTerm(Engine, goal, parameter.variableName)
  }

  private def theories: Theory = {
    val theoryUnion = (theory1: String, theory2: String) => theory1 concat "" concat theory2
    val theory = loadAllTheories(pathTheories: String).foldRight(" ")(theoryUnion)
    stringToTheory(theory)
  }

  protected case class Goal(predicateName: String, variableName: String)

  private val Engine = makePrologEngine(theories)

}


trait KdTreeEngine {
  def constructKdTree(dataSet: Term): Option[KdTree]

  sealed trait SubTree
  case object Left extends SubTree
  case object Right extends SubTree
}


private class DefaultKdTreeEngine extends PrologEngine with KdTreeEngine {

  private lazy val sourceDirectory: String = Paths.get(System.getProperty("user.dir"), "src").toString
  override protected lazy val pathTheories: String = Paths.get(sourceDirectory, "main", "prolog", "construction").toString

  override def constructKdTree(dataSet: Term): Option[KdTree] = {
    val prologKdTree = satisfyGoal(KdTreeGoal, dataSet)
    kdTreeConstruction(prologKdTree)
  }

  private def kdTreeConstruction(kdTree: Option[Term]): Option[KdTree] = kdTree match {
    case Some(prologKdTree) => extractNode(prologKdTree) match {
      case Some(node) if node.cut.isDefined =>
        val prologLeftSubTree = extractSubTree(prologKdTree, Left)
        val prologRightSubTree = extractSubTree(prologKdTree, Right)
        Some(KdTree(kdTreeConstruction(prologLeftSubTree), node, kdTreeConstruction(prologRightSubTree)))
      case Some(node) => Some(KdTree(None, node, None))
      case _ => None
    }
    case _ => None
  }

  private def extractNode(kdTree: Term): Option[Node] = satisfyGoal(NodeGoal, kdTree) match {
    case Some(node) if !node.isAtom =>
      val prologPattern = extractPattern(node)
      val pattern = prologListToSeq(prologPattern).map(string => string.toInt)
      Some(Node(pattern, extractCut(node)))
    case _ => None
  }

  private def extractPattern(node: Term): Seq[Int] = satisfyGoal(PatternGoal, node) match {
    case Some(pattern) => prologListToSeq(pattern).map(string => string.toInt)
    case _ => Seq.empty
  }

  private def extractCut(node: Term): Option[Cut] = satisfyGoal(CutGoal, node) match {
    case Some(cut) =>
      val k = prologListToSeq(cut).map(string => string.toInt)
      Some(Cut(k.head, k.tail.head))
    case _ => None
  }

  private def extractSubTree(kdTree: Term, subtree: SubTree): Option[Term] =
    satisfyGoal(subtree match {
      case Left => LeftSubTreeGoal
      case _ => RightSubTreeGoal
    }, kdTree)

  private object CutGoal extends Goal("extract_cut", "Cut")
  private object KdTreeGoal extends Goal("kd_tree", "Kd_Tree")
  private object NodeGoal extends Goal("extract_node", "Node")
  private object PatternGoal extends Goal("extract_pattern", "Pattern")
  private object LeftSubTreeGoal extends Goal("extract_left_tree", "Left_Subtree")
  private object RightSubTreeGoal extends Goal("extract_right_tree", "Right_Subtree")
}

object KdTreeEngine {
  def apply(): KdTreeEngine = new DefaultKdTreeEngine()
}
