trait KdTreeFactory {
  private val SingleNeighbor = 1

  def kdTree(pathDataSet: String): Option[KdTree]

  def kdTree(patterns: Stream[Seq[Int]]): Option[KdTree]

  def nearestNeighbor(): Stream[List[Int]] =
    nearestNeighbors(SingleNeighbor)

  def nearestNeighbors(k: Int): Stream[List[Int]]
}

object KdTreeFactory {

  def apply(): KdTreeFactory = new KdTreeFactory with DataSetReader {
    import ImplicitDefaultParser._
    import ScalaToProlog._

    override def kdTree(pathDataSet: String): Option[KdTree] = {
      val patterns = loadDataSet(pathDataSet)
      kdTree(patterns)
    }

    override def kdTree(patterns: Stream[Seq[Int]]): Option[KdTree] = {
      val dataSet = seqToTerm(patterns.map(pattern => seqToTerm(pattern)))
      Engine.constructKdTree(dataSet)
    }

    override def nearestNeighbors(k: Int): Stream[List[Int]] = ???
  }

  private val Engine = KdTreeEngine()
}
