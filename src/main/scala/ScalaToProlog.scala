import alice.tuprolog._

/** Implicit conversions and helpers for alice.tuprolog */
object ScalaToProlog {

  type Engine = Term => Stream[SolveInfo]

  /** Take a string and create a tuProlog Term
    *
    * @param term a valid Prolog term, e.g. search(a,[a,b,c])
    */
  implicit def stringToTerm(term: String): Term = Term.createTerm(term)

  /** Take a sequence and create a tuProlog Term
    *
    * @param termSequence a sequence of element that will form a Prolog list
    */
  implicit def seqToTerm[T](termSequence: Seq[T]): Term = termSequence.mkString("[", ",", "]")

  /** Take a Prolog Term that represent a Prolog list and create a Seq of String
    *
    * @param prologList a Prolog Term that represent a Prolog list
    */
  implicit def prologListToSeq(prologList: Term): Seq[String] =
    prologList.toString.replace("[", "").replace("]", "").split(",").toSeq

  /** Take a string and create a tuProlog theory
    *
    * @param theory a valid Prolog theory, e.g.
    *               search(E,[E|_]).
    *               search(E,[_|L]):-search(E,L).
    */
  implicit def stringToTheory[T](theory: String): Theory = new Theory(theory)

  /** Prepare a tuProlog engine
    *
    * @param theory a Prolog DB, as a text or list of clauses
    * @return a function that take a goal (a Prolog term)
    *         and pruduce a stream of solution (the outcome of a goal)
    */
  def makePrologEngine(theory: Theory): Engine = {
    val engine = new Prolog
    engine.setTheory(theory)

    goal =>
      new Iterable[SolveInfo] {

        override def iterator: Iterator[SolveInfo] = new Iterator[SolveInfo] {
          var solution: Option[SolveInfo] = Some(engine.solve(goal))

          override def hasNext: Boolean = solution.isDefined &&
            (solution.get.isSuccess || solution.get.hasOpenAlternatives)

          override def next(): SolveInfo =
            try solution.get
            finally solution = if (solution.get.hasOpenAlternatives) Some(engine.solveNext()) else None
        }
      }.toStream
  }

  /** Prepare a basic tuProlog engine, without theory
    *
    */
  def makePrologEngine: Engine = makePrologEngine("")

  /** query a Prolog engine with goal and returns if succeeds
    *
    * @param engine a Prolog engine produced with makePrologEngine
    * @param goal   a tuProlog term to be solved
    * @return true if goal has at least one result, false otherwise
    */
  def solveWithSuccess(engine: Engine, goal: Term): Boolean =
    engine(goal).map(_.isSuccess).headOption.nonEmpty

  /** query a Prolog engine with goal and returns the solution
    *
    * @param engine a Prolog engine produced with makePrologEngine
    * @param goal   a tuProlog goal to be solved
    * @param term   the variable name to get from the goal solution
    * @return The solution of goal
    */
  def solveOneAndGetTerm(engine: Engine, goal: Term, term: String): Option[Term] =
    engine(goal).headOption map (extractTerm(_, term))

  /** query a Prolog engine with goal and returns the solution
    *
    * @param engine    a Prolog engine produced with makePrologEngine
    * @param goal      a tuProlog goal to be solved
    * @param termIndex the variable name to get from the goal solution
    * @return The solution of goal
    */
  def solveOneAndGetTerm(engine: Engine, goal: Term, termIndex: Integer): Option[Term] =
    engine(goal).headOption map (extractTerm(_, termIndex))

  /** Extract term from solveInfo
    *
    * @param solveInfo the tuProlog solution
    * @param termIndex the index of the term in the goal that have produced solveInfo
    * @return the term at position i
    */
  def extractTerm(solveInfo: SolveInfo, termIndex: Integer): Term =
    solveInfo.getSolution.asInstanceOf[Struct].getArg(termIndex).getTerm

  /** Extract the referred term from solveInfo
    *
    * @param solveInfo the tuProlog solution
    * @param s         the string that refers the term in the goal that have produced solveInfo
    * @return the term identified by s
    */
  def extractTerm(solveInfo: SolveInfo, s: String): Term =
    solveInfo.getTerm(s)

}
