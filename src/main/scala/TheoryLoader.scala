import java.nio.file.Paths

import scala.io.Source

trait TheoryLoader {

  def loadAllTheories(directoryPath: String): Seq[String] = {
    Paths.get(directoryPath).toFile
      .listFiles.filter(file => file.isFile && file.getName.endsWith(".pl"))
      .map(path => loadTheory(path.getPath))
  }

  def loadTheory(path: String): String = {
    val reader = Source.fromFile(path)
    val theory = reader.getLines.reduce((t: String, u: String) => t + "\n" + u)
    reader.close()
    theory
  }

}
