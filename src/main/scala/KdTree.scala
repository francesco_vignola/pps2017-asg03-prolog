import java.nio.file.Paths

case class Cut(attribute: Int, value: Int)
case class Node(pattern: Seq[scala.Int], cut: Option[Cut])
case class KdTree(leftSubTree: Option[KdTree],
                  value: Node, rightSubTree: Option[KdTree])


object KdTreeTest extends App {

  val sourceDirectory: String = Paths.get(System.getProperty("user.dir"), "src").toString
  val pathDataSet: String = Paths.get(sourceDirectory, "test", "resources", "dataset.txt").toString

  private val engine = KdTreeFactory()
  println(engine.kdTree(pathDataSet))
}