import scala.util.matching.Regex

object ImplicitDefaultParser {
  private val pattern: String = "^\\[\\d+(,\\s*\\t*\\d+)*\\]$"
  private val features: Regex = "\\d+".r

  implicit def dataPointParser(line: String): Seq[String] =
    features.findAllIn(line).toSeq

  implicit def dataSetParser(dataSet: Iterator[String]): Stream[Seq[String]] =
    dataSet.filter(line => line.matches(pattern))
      .map(line => dataPointParser(line)).toStream

}
