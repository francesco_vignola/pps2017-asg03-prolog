import scala.io.Source

trait DataSetReader {

  def loadDataSet(pathDataSet: String)(implicit parser: Iterator[String] => Stream[Seq[String]]): Stream[Seq[Int]] = {
    parser(Source.fromFile(pathDataSet).getLines).map(sequence => sequence.map(string => string.toInt))
  }

}
